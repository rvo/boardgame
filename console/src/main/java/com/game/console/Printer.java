package com.game.console;

public interface Printer {

    void print(String line);

}
