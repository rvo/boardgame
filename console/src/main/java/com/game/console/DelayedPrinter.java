package com.game.console;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.PrintStream;

public class DelayedPrinter implements Printer {

    private Speed speed;
    private PrintStream out;

    enum Speed {

        fast(1), slow(50);

        private int delay;

        private Speed(int delay) {
            this.delay = delay;
        };

        public int getDelay() {
            return delay;
        }
    }

    public DelayedPrinter(PrintStream out) {

        this(out, Speed.slow);
    }

    public DelayedPrinter(PrintStream out, Speed speed) {

        if (null == out || null == speed) {
            String msg = "PrintStream and Speed must be null";
            throw new IllegalArgumentException(msg);
        }

        this.out = out;
        this.speed = speed;
    }

    @Override
    public void print(String line) {

        if (null == line) {
            return;
        }

        for (char c : line.toCharArray()) {

            out.print(c);
            try {
                MILLISECONDS.sleep(speed.getDelay());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        out.println();
    }

}
