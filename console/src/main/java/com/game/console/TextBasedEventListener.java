package com.game.console;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.game.core.api.Character;
import com.game.core.api.EventListener;

public class TextBasedEventListener implements EventListener {

    private Printer printer;

    public TextBasedEventListener(Printer printer) {

        if (null == printer) {
            String msg = "Printer must be null";
            throw new IllegalArgumentException(msg);
        }

        this.printer = printer;
    }

    @Override
    public void gameStarted(List<Character> characters) {

        print("A new game with the following characters starts!");
        print(" > " + StringUtils.join(characters, ", "));
    }

    @Override
    public void nextMove(Character active) {

        print("..........");
        print("It's the " + active + "'s turn.");
    }

    @Override
    public void moveFinished(Character attacked, int damage) {

        print("The " + attacked + " suffered " + damage + " damage.");

        String msg;
        if (!attacked.isDead()) {
            int health = attacked.getHealth();
            msg = "He has " + health + " health points left.";
        } else {
            msg = "Unfortunately the " + attacked + " died.";
        }
        print(" > " + msg);
    }

    @Override
    public void gameFinished(Character winner) {

        print("\n**********");
        print("The incredible " + winner + " wins!");
        print("\n");
    }

    private void print(String line) {

        printer.print(line);
    }

}
