package com.game.core.api;

import java.util.List;

/**
 * Implement this interface to get informed about the state of a {@link Game}.
 * 
 * @author Ronny
 */
public interface EventListener {

    /**
     * Get's fired immediately after a new game was started.
     * 
     * @param characters
     *            List of characters that are in the game in order of their
     *            appearance.
     */
    void gameStarted(List<Character> characters);

    /**
     * Get's fired immediately before a new move will be executed.
     * 
     * @param active
     *            Character that will be "moved".
     */
    void nextMove(Character active);

    /**
     * Get's fired immediately after a move was executed.
     * 
     * @param attacked
     *            the character that was attacked
     * @param damage
     *            amount of life energy that the attacked character lost
     */
    void moveFinished(Character attacked, int damage);

    /**
     * Get's fired immediately after a game was finished.
     * 
     * @param winner
     *            the only character that is left on the game board.
     */
    void gameFinished(Character winner);
}
