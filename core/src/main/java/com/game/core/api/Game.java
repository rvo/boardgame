package com.game.core.api;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.game.core.util.Board;
import com.game.core.util.Color;
import com.game.core.util.EventDispatcher;
import com.game.core.util.Robot;
import com.game.core.util.SimpleDice;

/**
 * <p>
 * The class contains the game logic and controls it's flow. It's implemented as
 * {@link Thread}, so it can be paused between user interactions.
 * </p>
 * 
 * <p>
 * Initialize a game object through an appropriate constructor or through
 * different add methods. Start a new game {@link Thread} with the
 * {@link #start()} methods. The Thread runs until the game is finished. Use the
 * {@link #interrupt()} method to quit a game thread before the game ends.
 * </p>
 * 
 * <p>
 * {@link EventListener}s can be added, to get informed about the state of the
 * game.
 * </p>
 * 
 * @author Ronny
 */
public class Game extends Thread {

    /**
     * Maximum number of {@link Character}s per game instance.
     */
    public static final int CHARACTER_MAX = Color.values().length;

    /**
     * Minimum number of {@link Character}s per game instance.
     */
    public static final int CHARACTER_MIN = 2;

    /**
     * Maximum value of {@link Character}s life energy.
     */
    public static final int HEALTH_MIN = 5;

    /**
     * Maximum value of {@link Character}s life energy.
     */
    public static final int HEALTH_MAX = 25;

    private Map<Character, Player> players = new LinkedHashMap<Character, Player>();

    private Board board = new Board();

    private EventDispatcher dispatcher = new EventDispatcher();

    /**
     * Creates an empty game. Characters must be added to the game through on of
     * the {@link #add(Character)} methods.
     */
    public Game() {
        // default
    }

    /**
     * Creates a new game and all objects that are required to start it
     * immediately.
     * 
     * @param players
     *            number of players that attend the game
     * @param health
     *            initial life energy of all characters
     * @param delay
     *            delay in seconds before a new move is made
     * 
     */
    public Game(int players, int health, int delay) {

        setName("Game");

        Color[] colors = Color.values();
        if (colors.length < players) {
            throw new IllegalArgumentException("not enough colors");
        }

        for (int i = 0; i < players; i++) {

            Character character = new Knight(colors[i], health);
            Dice dice = new SimpleDice();
            Player robot = new Robot(dice, delay);
            add(character, robot);
        }
    }

    /**
     * Same as {@link #Game()} but without any delay.
     * 
     * @see #Game(int, int)
     */
    public Game(int players, int health) {

        this(players, health, -1);
    }

    @Override
    public void run() {

        Character winner = runGame();

        if (null == winner) {
            System.err.println("Game was interrupted.");
        }
    }

    /**
     * Let all {@link Character}s fight each other until only one
     * {@link Character} is left.
     * 
     * @return the last character the remains on the game {@link Board}
     */
    Character runGame() {

        checkPreconditions();
        gameStarted();
        Character active = board.nextCharacter();

        while (board.countCharacters() >= CHARACTER_MIN) {

            nextMove(active);
            Character opponent = board.nextCharacter();
            Player activePlayer = players.get(active);

            int damage = opponent.takeHit(activePlayer.play());
            moveFinished(opponent, damage);

            if (opponent.isDead()) {
                board.removeCharacter(opponent);
                active = board.nextCharacter();
            } else {
                active = opponent;
            }
            if (isInterrupted()) {
                return null;
            }
        }

        gameFinished(active);
        return active;
    }

    /**
     * Adds the given {@link Character} to this game and connects it with an
     * {@link Robot} player, that has a {@link SimpleDice}.
     * 
     */
    public void add(Character c) {

        add(c, new Robot(new SimpleDice()));
    }

    /**
     * Add a {@link Character} and a {@link Player} to this game and connects
     * each other.
     */
    public void add(Character c, Player p) {

        players.put(c, p);
        board.addCharacter(c);
    }

    public void addListener(EventListener listener) {

        dispatcher.add(listener);
    }

    private void gameStarted() {

        List<Character> allCharacters;
        allCharacters = new ArrayList<Character>(players.keySet());
        dispatcher.gameStarted(allCharacters);

    }

    private void nextMove(Character active) {

        dispatcher.nextMove(active);
    }

    private void moveFinished(Character opponent, int damage) {

        dispatcher.moveFinished(opponent, damage);
    }

    private void gameFinished(Character active) {

        dispatcher.gameFinished(active);
        cleanUp();
    }

    private void cleanUp() {

        players.clear();
        board.clear();
    }

    private void checkPreconditions() {

        if (board.countCharacters() < 2) {
            String msg = "At least two players are required for this game";
            throw new IllegalStateException(msg);
        }
    }

}
