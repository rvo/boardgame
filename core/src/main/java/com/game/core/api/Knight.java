package com.game.core.api;

import com.game.core.util.Color;

/**
 * Character implementation. Could extend the general behavior of the
 * {@link Character} later.
 * 
 * @author Ronny
 */
public class Knight extends Character {

    /**
     * Default health of {@value} points.
     */
    private static final int DEFAULT_HEALTH = 25;

    /**
     * Creates a default knight with black color and {@value #DEFAULT_HEALTH}
     * health points.
     */
    public Knight() {

        this(Color.black, DEFAULT_HEALTH);
    }

    /**
     * Creates an knight with the given properties.
     */
    public Knight(Color color, int health) {

        super("knight", color, health);
    }

}
