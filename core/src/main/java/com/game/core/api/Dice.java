package com.game.core.api;

public interface Dice {

    int roll();
}
