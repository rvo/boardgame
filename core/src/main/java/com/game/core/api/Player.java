package com.game.core.api;

/**
 * Represents the Entity, that controls a {@link Character}. Could be a person
 * or bot for example.
 * 
 * @author Ronny
 */
public interface Player {

    int play();
}
