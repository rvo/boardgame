package com.game.core.api;

import com.game.core.util.Color;

/**
 * A character has a type, a color and life energy (health). Characters can
 * fight each other and loose energy.
 * 
 * @author Ronny
 */
public abstract class Character {

    private final String type;

    private final Color color;

    private int health;

    /**
     * Create a new {@link Character} instance with the given properties.
     */
    Character(String type, Color color, int health) {

        if (null == color || health < 0) {
            String msg = "Color must not be empty and health must be greater than zero.";
            throw new IllegalArgumentException(msg);
        }

        this.type = type;
        this.color = color;
        this.health = health;
    }

    /**
     * Reduces the health of this character by the given strength.
     * 
     * @param strength
     *            the strength of the attack
     * @return the strength of the attack
     */
    public int takeHit(int strength) {

        if (strength < 0) {
            String msg = getName() + " can't handle negative hits!";
            throw new IllegalArgumentException(msg);
        }

        if (isDead()) {
            String msg = getName() + " is already dead!";
            throw new IllegalStateException(msg);
        }

        health -= strength;
        return strength;
    }

    public String getType() {
        return type;
    }

    public Color getColor() {
        return color;
    }

    public int getHealth() {
        return health;
    }

    /**
     * 
     * @return true - if health is equal or less than zero<br>
     *         false - otherwise
     */
    public boolean isDead() {
        return 0 >= getHealth();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Character other = (Character) obj;
        if (color != other.color)
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getName();
    }

    private String getName() {
        return getColor() + " " + getType();
    }

}
