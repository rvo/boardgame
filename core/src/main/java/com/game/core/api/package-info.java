/**
 * Representation of the game logic. 
 * Classes in this package can be used by client modules to create and run board game simulations.
 */
package com.game.core.api;