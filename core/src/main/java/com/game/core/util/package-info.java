/**
 * Utility classes which are required for game handling. Should be used only internally by core module and 
 * not accessed from other modules.
 */
package com.game.core.util;