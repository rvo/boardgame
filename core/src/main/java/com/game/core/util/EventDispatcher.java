package com.game.core.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.game.core.api.Character;
import com.game.core.api.EventListener;

public class EventDispatcher {

    private Collection<EventListener> listeners = new LinkedList<EventListener>();

    public void moveFinished(final Character attacked, final int damage) {

        for (EventListener l : listeners) {
            l.moveFinished(attacked, damage);
        }
    }

    public void nextMove(Character active) {

        for (EventListener l : listeners) {
            l.nextMove(active);
        }
    }

    public void gameFinished(Character winner) {

        for (EventListener l : listeners) {
            l.gameFinished(winner);
        }
    }

    public void gameStarted(List<Character> allCharacters) {

        for (EventListener l : listeners) {
            l.gameStarted(allCharacters);
        }
    }

    public void add(EventListener listener) {

        listeners.add(listener);
    }

}
