package com.game.core.util;

import com.game.core.api.Dice;

public class SimpleDice implements Dice {

    @Override
    public int roll() {

        return (int) (Math.random() * 6) + 1;
    }

}
