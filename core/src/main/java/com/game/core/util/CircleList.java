package com.game.core.util;

import java.util.LinkedList;
import java.util.List;

public class CircleList<E> {

    private List<E> delegate = new LinkedList<E>();
    private int index = 0;

    public boolean add(E e) {

        return delegate.add(e);
    }

    public boolean remove(E o) {

        int remove = delegate.indexOf(o);

        if (remove < index) {
            index--;
        }
        return delegate.remove(o);
    }

    public int size() {

        return delegate.size();
    }

    public boolean contains(E o) {

        return delegate.contains(o);
    }

    public E next() {

        if (size() == 0) {
            String msg = "CircleList is empty";
            throw new IllegalStateException(msg);
        }

        if (size() == index) {
            index = 0;
        }
        return delegate.get(index++);
    }
}
