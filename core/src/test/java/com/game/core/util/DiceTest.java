package com.game.core.util;

import static org.testng.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import com.game.core.api.Dice;

/**
 * Testcase for different {@link Dice} implementations.
 * 
 * @author Ronny
 */
public class DiceTest {

    /**
     * Make sure that @link SimpleDice} returns numbers between 1 and 6.
     */
    @Test
    public void testSimpleDice() {

        Dice dice = new SimpleDice();
        Set<Integer> result = new HashSet<Integer>();

        for (int i = 0; i < 100; i++) {
            result.add(dice.roll());
        }

        for (int i = 1; i <= 6; i++) {
            assertTrue(result.contains(i));
        }

    }

}
