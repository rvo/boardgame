package com.game.core.util;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.testng.annotations.Test;

import com.game.core.api.EventListener;

/**
 * Mockito based Testcase for {@link EventDispatcher}
 * 
 * @author Ronny
 */
public class EventDispatcherTest {

    /**
     * Make sure {@link EventDispatcher} forwards events to all it's
     * {@link EventListener}s.
     */
    @Test
    public void testFireEvents() {

        EventListener listener = mock(EventListener.class);

        EventDispatcher d = new EventDispatcher();
        d.add(listener);
        d.add(listener);

        d.gameStarted(null);
        d.moveFinished(null, 0);
        d.nextMove(null);
        d.gameFinished(null);

        verify(listener, times(2)).gameStarted(null);
        verify(listener, times(2)).moveFinished(null, 0);
        verify(listener, times(2)).nextMove(null);
        verify(listener, times(2)).gameFinished(null);
    }
}
