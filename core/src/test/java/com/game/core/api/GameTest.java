package com.game.core.api;

import static org.testng.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import com.game.core.util.Color;

/**
 * Testcase for {@link Game}.
 * 
 * @author Ronny
 */
public class GameTest {

    /**
     * Ensure the game thread handles {@link Thread#interrupt()}. Initialize a
     * very long game and cancel it immediately.
     */
    @Test(timeOut = 1000)
    public void testInterrupt() throws InterruptedException {

        Thread game = new Game(5, 100, 100);
        game.start();
        game.interrupt();
        game.join();
    }

    @Test
    public void testGame() throws InterruptedException {

        Game game = new Game();
        Set<Character> winners = new HashSet<Character>();

        for (int i = 0; i < 500; i++) {

            for (Color color : Color.values()) {
                game.add(new Knight(color, 25));
            }

            winners.add(game.runGame());
        }

        // make sure that every character has at least won one game
        assertEquals(winners.size(), Color.values().length);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void testEmptyGame() throws InterruptedException {

        Game game = new Game();
        game.add(new Knight());
        game.runGame();
    }

}
