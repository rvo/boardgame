package com.game.server;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.game.server.handler.GameStartHandler;
import com.game.server.handler.StaticResourceHandler;
import com.sun.net.httpserver.HttpServer;

public class ServerApp {

    private static final int DEFAUL_PORT = 8000;

    public static void main(String[] args) throws IOException {

        int port = getPort(args);

        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new StaticResourceHandler());
        server.createContext("/game/start", new GameStartHandler());
        server.start();

        System.out.println("Server is listening at http://localhost:" + port);
        System.out.println("Press Enter to quit server");

        System.in.read();
        server.stop(0);
        System.out.println("Server down");
    }

    private static int getPort(String[] args) {

        if (args.length == 0) {
            return DEFAUL_PORT;
        }

        try {
            return Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            return DEFAUL_PORT;
        }
    }

}
