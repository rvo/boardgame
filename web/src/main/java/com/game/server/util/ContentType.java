package com.game.server.util;

import java.util.HashMap;
import java.util.Map;

public enum ContentType {

    HTML("text/html", "html", "htm"),

    JAVA_SCRIPT("text/javascript", "js"),

    JPG("image/jpg", "jpg");

    private static final Map<String, ContentType> registry = new HashMap<String, ContentType>();

    private final String type;

    private final String[] extensions;

    private ContentType(String type, String... extensions) {

        this.type = type;
        this.extensions = extensions;
    }

    @Override
    public String toString() {
        return type;
    }

    public static ContentType get(String resource) {

        if (registry.isEmpty()) {
            initRegistry();
        }

        String extension = getExtension(resource);
        if (null == extension) {
            return null;
        }
        return registry.get(extension);

    }

    private static String getExtension(String resource) {

        int dot = resource.lastIndexOf(".");

        if (-1 == dot) {
            return null;
        }
        return resource.substring(dot + 1, resource.length());
    }

    private static void initRegistry() {

        for (ContentType t : ContentType.values()) {

            for (String extension : t.extensions) {
                registry.put(extension, t);
            }
        }
    }

}
